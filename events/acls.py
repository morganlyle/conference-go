# import json

# # import requests

# from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


# def get_photo(city, state):
#     # Create a dictionary for the headers to use in the request
#     headers = {"Authorization": PEXELS_API_KEY}
#     params = {
#         "per_page": 1,
#         "query": city + " " + state,
#     }
#     # Create the URL for the request with the city and state
#     # Make the request
#     url = "https://api.pexels.com/v1/search"
#     response = requests.get(url, params=params, headers=headers)
#     content = json.loads(response.content)
#     # Return a dictionary that contains a `picture_url` key and
#     #   one of the URLs for one of the pictures in the response
#     try:
#         return {"picture_url": content["photos"][0]["src"]["original"]}
#     except:
#         return {"picture_url": None}


# def get_weather_data(city, state):
#     # Create the URL for the geocoding API with the city and state
#     # Make the request
#     params = {
#         "weather": f"{city} {state}",
#         "limit": 1,
#         "appid": OPEN_WEATHER_API_KEY,
#     }
#     # Parse the JSON response
#     # Get the latitude and longitude from the response
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     response = requests.post(url, params=params)
#     content = json.loads(response.content)
#     # lat = content[0]["lat"]
#     # lon = content[0]["lon"]

#     # Create the URL for the current weather API with the latitude
#     #   and longitude
#     # Make the request
#     # Parse the JSON response
#     # Get the main temperature and the weather's description and put
#     #   them in a dictionary
#     # Return the dictionary
